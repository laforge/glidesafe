/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.flightinfo.FullFlight;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;

import java.util.ArrayList;
import java.util.List;

public class FlightVisualization extends AppCompatActivity {

    public static final String RECEIVE_HEART_RATE_LOCATION = "RECEIVE_HEART_RATE_LOCATION";
    public static final String HEART_RATE = "HEART_RATE";
    public static final String HEIGHT = "HEIGHT";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String LATITUDE = "LATITUDE";
    public static final String SPEED = "SPEED";
    public static final String ACC = "ACC";
    public static final String END_FLIGHT = "END_FLIGHT";
    private static final String TAG = "FLIGHTVIS";


    MapView map = null;

    @Override public void onCreate(Bundle savedInstanceState) {
        Configuration.getInstance().setUserAgentValue(getPackageName());
        super.onCreate(savedInstanceState);

        //load/initialize the osmdroid configuration, this can be done
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string

        //inflate and create the map

        setContentView(R.layout.activity_flight_visualization);

        map = findViewById(R.id.map);

        map.setTileSource(TileSourceFactory.MAPNIK);

        map.setMultiTouchControls(true);


        FullFlight flight = (FullFlight) getIntent().getSerializableExtra("flight");

        Polyline trace = new Polyline();
        List<GeoPoint> geoPoints = new ArrayList<>();

        double bNorth = flight.flightPoints.get(0).geoPoint.getLatitude();
        double bSouth = flight.flightPoints.get(0).geoPoint.getLatitude();
        double bEast = flight.flightPoints.get(0).geoPoint.getLongitude();
        double bWest = flight.flightPoints.get(0).geoPoint.getLongitude();

        for(int i = 0; i < flight.flightPoints.size(); i++){
            GeoPoint current = flight.flightPoints.get(i).geoPoint;
            if(bNorth < current.getLatitude()){
                bNorth = current.getLatitude();
            } else if (bSouth > current.getLatitude()){
                bSouth = current.getLatitude();
            }

            if(bEast < current.getLongitude()){
                bEast = current.getLongitude();
            } else if (bWest > current.getLongitude()){
                bWest= current.getLongitude();
            }

            geoPoints.add(flight.flightPoints.get(i).geoPoint);
        }

        trace.setPoints(geoPoints);
        trace.getPaint().setStrokeCap(Paint.Cap.ROUND);

        map.getOverlayManager().add(trace);

        map.invalidate();

        final double north = bNorth;
        final double south = bSouth;
        final double east = bEast;
        final double west =bWest;
        map.post(() -> {
            map.zoomToBoundingBox(new BoundingBox(north, east, south, west),
                    false,40, 18.5, (long)10);
        });
    }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));

        Log.v(TAG, "Zoom level: "+map.getZoomLevelDouble());

        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up

    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    public void stopRecordingOnWear() {

        Intent intentStopRec = new Intent(FlightVisualization.this, WearService.class);
        intentStopRec.setAction(WearService.ACTION_SEND.STOPACTIVITY.name());
        intentStopRec.putExtra(WearService.ACTIVITY_TO_STOP, BuildConfig.W_recordingactivity);
        startService(intentStopRec);

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this);

                // Retrieve HR data
                ArrayList<Integer> hrArray = intent.getIntegerArrayListExtra(HEART_RATE);
                // Retrieve Height data
                ArrayList<Integer> heightArray = intent.getIntegerArrayListExtra(HEIGHT);
                // Retrieve speed data
                ArrayList<Integer> speedtArray = intent.getIntegerArrayListExtra(SPEED);
                // Retrieve speed data
                ArrayList<Integer> acctArray = intent.getIntegerArrayListExtra(ACC);

                // Convert the primitive float[] to a List of Float objects for Firebase
                // for the latitudes and longitudes
                float[] array = intent.getFloatArrayExtra(LATITUDE);
                List<Float> latArray = new ArrayList<>(array.length);
                for (float f : array) latArray.add(f);
                array = intent.getFloatArrayExtra(LONGITUDE);
                List<Float> lonArray = new ArrayList<>(array.length);
                for (float f : array) lonArray.add(f);

                // Upload everything in Firebase
/*                recordingRef.child("hr_watch").setValue(hrArray);
                recordingRef.child("loc_lat_watch").setValue(latArray);
                recordingRef.child("loc_lon_watch").setValue(lonArray);*/
                int listSize = hrArray.size();

                for (int i = 0; i<listSize; i++){
                    Log.d(TAG, "height :" + hrArray.get(i));
                }

            }
        }, new IntentFilter(RECEIVE_HEART_RATE_LOCATION));

        finish();
    }
}
