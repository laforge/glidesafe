/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class FirebaseUtils {
    // add a single timestamp 'timestamp' with flight data 'flight_data' to the flight with name 'flight_name'
    public static void uploadTimestampToDB(final String flight_name, final String timestamp, final String flight_data){
        // fetch user id
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        Log.d(MainActivity.TAG, "Starting database upload with uid: " + user.getUid());
        // get reference of DB of user
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference(user.getUid());
        userRef.runTransaction(new Transaction.Handler(){
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                // add new data to DB
                mutableData.child("flight_data").child(flight_name).child(timestamp).setValue(flight_data);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (b) {
                    Log.d(MainActivity.TAG, "Database upload successful");
                } else {
                    Log.d(MainActivity.TAG, "Database upload failed: " + databaseError.getMessage());
                }
            }
        });
    }

    // do something with the retrieved list of timestamps and flight data
    private static void processRetrievedData(ArrayList<String> dataList){
        Log.d(MainActivity.TAG, "Retrieved data:");
        for(int i = 0; i < dataList.size(); i++){
            Log.d(MainActivity.TAG, dataList.get(i));
        }
    }

    public static void uploadFileToFB(Uri fileUri, String flightName){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        // Create a storage reference from our app
        StorageReference storageRef = FirebaseStorage.getInstance().getReference()
                .child("flight data").child(user.getUid());
        StorageReference flightRef = storageRef.child(flightName);
        UploadTask uploadTask = flightRef.putFile(fileUri);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.d(MainActivity.TAG, "Flight upload failed");
                Toast.makeText(MyApplication.getContext(),"Flight upload failed", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(MainActivity.TAG, "Flight upload successful");
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                Log.d(MainActivity.TAG, "Upload is " + progress + "% done");
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(MainActivity.TAG, "Upload is paused");
            }
        });
    }

    public static void downloadFileFromFB(Context context, String flightName){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }

        ContextWrapper c = new ContextWrapper(context);
        String FileDirectory =  c.getFilesDir().getPath();
        File flightRootFolder = new File(FileDirectory, "Flight");

        if (!flightRootFolder.exists()) {
            if(!flightRootFolder.mkdir()){
                return;
            }
        } else if (!flightRootFolder.isDirectory()) {
            Log.e("SafeGlide", "FlightRootFolder exist but is not a folder. Not saving flight");
            return;
        }

        File flightSaveFolder = new File(flightRootFolder, user.toString());
        if (!flightSaveFolder.exists()) {
            if(flightSaveFolder.mkdir()){
                return;
            }
        } else if (!flightSaveFolder.isDirectory()){
            Log.e("SafeGlide", "FlightSaveFolder exist but is not a folder. Not saving flight");
            return;
        }

        final File localFile = new File(flightSaveFolder, flightName);

        // Get the reference to the file
        StorageReference storageRef = FirebaseStorage.getInstance().getReference()
                .child("flight data").child(user.getUid());
        StorageReference flightRef = storageRef.child(flightName);


        flightRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // Local temp file has been created
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(localFile));
                    Log.d(MainActivity.TAG, reader.readLine());
                    reader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
                Log.d(MainActivity.TAG, "Flight download successful: ");
                // put the method that processes the flight data here!
                // processFlightData(localFile)
                // --------------------------------------------------------
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful downloads
                Toast.makeText(MyApplication.getContext(),"Flight download failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // download the list of all flights of the current user from DB
    // be sure to put your method that processes the retrieved list where indicated
    public static void syncFB(Context context){
        final ArrayList<String> outputList = new ArrayList<>();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        Log.d(MainActivity.TAG, "Retrieving flights of uid " + user.getUid());
        // Get the reference to storage
        final StorageReference storageRef = FirebaseStorage.getInstance().getReference()
                .child("flight data").child(user.getUid());
        storageRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for (StorageReference item : listResult.getItems()) {
                            // All the prefixes under listRef.
                            outputList.add(item.getName());
                        }
                        // put the method that processes the flight list here!
                        processFlightListFB(context, outputList);
                        // ---------------------------------------------------
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Uh-oh, an error occurred!
                    }
                });
    }
    public static void processFlightListFB(Context context, ArrayList<String> outputList){
        ContextWrapper c = new ContextWrapper(context);

        Path flightRoot =  (new File(c.getFilesDir(),"Flight")).toPath();

        ArrayList<File> localFile = new ArrayList<>();
        try {
            Files.walk(flightRoot)
                    .filter(Files::isRegularFile)
                    .forEach(f -> {
                        localFile.add((new File(f.toString())));
                    });
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        ArrayList<String> localFlights = new ArrayList<>();

        for(int i = 0; i < localFile.size(); i++){
            localFlights.add(localFile.get(i).getName());
        }

        for(int i = 0; i < outputList.size(); i++){
            if(!localFlights.contains(outputList.get(i))){
                Log.d(MainActivity.TAG, "Downloading file: " + outputList.get(i));
                downloadFileFromFB(context, outputList.get(i));
            } else if (!outputList.contains(localFlights.get(i))){
                FirebaseUtils.uploadFileToFB(Uri.fromFile(localFile.get(i)), localFlights.get(i));
            }
        }
    }

}
