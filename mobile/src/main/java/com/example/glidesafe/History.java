/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.flightinfo.DataPoint;
import com.example.flightinfo.FullFlight;
import com.example.flightinfo.RunnableFlight;

import org.osmdroid.util.GeoPoint;

import java.io.Serializable;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static com.example.glidesafe.MyApplication.getContext;

public class History extends Activity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;

    private ArrayList<FullFlight> myDataset;

    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        // generating random flight
        Random rand = new Random();
        double x=46.29718;
        double y = 7.02300;
        FullFlight ta = new FullFlight();
        for(int i = 0; i < 100; i++){
            ta.flightPoints.add(new DataPoint(Instant.now(),new GeoPoint(x,y,0),0,0,0));
            x+=(rand.nextDouble()-0.5)*0.01;
            y+=(rand.nextDouble()-0.5)*0.01;
        }

        ta.flightPoints.add(new DataPoint(Instant.now(),new GeoPoint(46.27730,6.99402,0),0,0,0));
        ta.setContext(this);
        ta.setUser(Profile.Name);
        ta.endFlight(new RunnableFlight(){
                                public void run(){
                                    FirebaseUtils.uploadFileToFB(this.getUri(), ta.getId());
                                }
                            });
*/
        context = this;

        setContentView(R.layout.activity_history);

        // Swipe containter part -----------------------------------------------------------------

        swipeLayout =  findViewById(R.id.swipe_container);
        // Refresh items
        swipeLayout.setOnRefreshListener(this::refreshItems);

        // RecyclerView --------------------------------------------------------------------------

        recyclerView = findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        myDataset = FullFlight.getFlights(this);

        // specify an adapter
        recyclerView.setAdapter(new HistoryAdapter(myDataset, item -> {
            Intent intent = new Intent(this, FlightVisualization.class);

            intent.putExtra("flight", item);
            startActivity(intent);
        }));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    void refreshItems() {
        // Load items
        Log.v("debug_tag", "Refreshing!");

        Toast toast=Toast.makeText(getApplicationContext(),"Syncing...",Toast.LENGTH_SHORT);
        toast.setMargin(50,50);
        toast.show();

        FirebaseUtils.syncFB(this);
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed


        // Stop refresh animation
        swipeLayout.setRefreshing(false);
    }
}
