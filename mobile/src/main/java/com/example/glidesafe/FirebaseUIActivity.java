/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FirebaseUIActivity extends AppCompatActivity {

    private static final int REAUTHENTICATE = 3;
    private FirebaseUser user;
    private static final int RC_SIGN_IN = 123;
    private static final int PICK_IMAGE = 4;

    private String photoPath = "";

    private Profile mProfile = new Profile();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_ui);

        // initialize loading bar to be off
        ProgressBar loading = findViewById(R.id.loadingProgressBar);
        loading.setVisibility(View.GONE);

        // initialize rest of layout with profile information
        Log.v(MainActivity.TAG, "creating FirebaseUIActivity");
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.v(MainActivity.TAG, "Signing in user...");
            signIn();
        }else {
            mProfile = (Profile) getIntent().getSerializableExtra("Profile");
            fillProfileInfo();
        }
    }

    private void fillProfileInfo(){
        TextView name = findViewById(R.id.name);
        TextView surname = findViewById(R.id.surname);
        TextView email = findViewById(R.id.email);
        name.setText(mProfile.Name.split(" ", 2)[0]);
        surname.setText(mProfile.Name.split(" ", 2)[1]);
        email.setText(mProfile.Email);
        ImageView imageView = findViewById(R.id.profileImage);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        StorageReference storageRef;
        if(user.getPhotoUrl() != null) {
            //  Reference to an image file in Firebase Storage
            storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(user.getPhotoUrl().toString());
            mProfile.ProfileImagePath = user.getPhotoUrl().toString();
        }else{
            String DEFAULT_PROFILE_IMAGE = "https://firebasestorage.googleapis.com/v0/b/glidesafe-e7088.appspot.com/o/profileImages%2Fplaceholder.jpg?alt=media&token=afe2badf-4042-4a2f-9ea9-6fbf18516d23";
            storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(DEFAULT_PROFILE_IMAGE);
            mProfile.ProfileImagePath = DEFAULT_PROFILE_IMAGE;
        }
        storageRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                final Bitmap ProfileImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                ImageView imageView = findViewById(R.id.profileImage);
                imageView.setImageBitmap(ProfileImage);
            }
        });
    }

    public void signIn(){
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    public void logOutCallback(View view){
        logOut();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("Status", "logged_out");
        resultIntent.putExtra("Profile", mProfile);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void logOut(){
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d(MainActivity.TAG, "Logout successful");
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data); // what to use this for?

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                Log.v(MainActivity.TAG, "Sign in complete");
                user = FirebaseAuth.getInstance().getCurrentUser();
                if(user == null){
                    Log.e(MainActivity.TAG, "Error: User is null");
                    return;
                }
                updateProfile();
                Toast.makeText(FirebaseUIActivity.this, "Welcome "
                        + user.getDisplayName().split(" ", 2)[0] + "!", Toast.LENGTH_LONG).show();
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                Log.v(MainActivity.TAG, "Sign in aborted");
                finish();
            }
        }else if (requestCode == REAUTHENTICATE) {
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                addProfileImageFirebaseDB();
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                Log.v(MainActivity.TAG, "Sign in aborted");
                finish();
            }
        }else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            // image chosen from phone
            Uri imageUri = data.getData();
            File imageFile = new File(getExternalFilesDir(null), "profileImage");
            try {
                // copy image from specified Uri
                copyImage(imageUri, imageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            final InputStream imageStream;
            try {
                Uri savedImageUri = Uri.fromFile(imageFile);
                imageStream = getContentResolver().openInputStream(Uri.fromFile(imageFile));
                // rotating and cropping image if necessary
                final Bitmap selectedImage = rotateImage(BitmapFactory.decodeStream(imageStream), savedImageUri);
                // put image into image view
                ImageView imageView = findViewById(R.id.profileImage);
                imageView.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateProfile(){
        getEmergencyContactFromDB();

        mProfile.Name = user.getDisplayName();
        mProfile.UID = user.getUid();
        mProfile.Email = user.getEmail();
        fillProfileInfo();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("Status", "logged_in");
        resultIntent.putExtra("Profile", mProfile);
        setResult(RESULT_OK, resultIntent);
    }

    public void saveChanges(View view){
        // turn on loading bar
        ProgressBar loading = findViewById(R.id.loadingProgressBar);
        loading.bringToFront();
        loading.setVisibility(View.VISIBLE);

        // check if user needs to re-authenticate
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(user.getDisplayName()).build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(MainActivity.TAG, "No re-authentication needed.");
                            // upload profileImage and completeSaveChanges
                            addProfileImageFirebaseDB();
                        } else {
                            Log.d(MainActivity.TAG, "Re-authentication needed.");
                            logOut();
                            // re-authenticate and continue saveChanges
                            reauthenticate();
                        }
                    }
                });
    }

    private void completeSaveChanges1() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        TextView password1 = findViewById(R.id.password1);
        TextView password2 = findViewById(R.id.password2);
        // update password
        if (!String.valueOf(password1.getText()).isEmpty()) {
            if (String.valueOf(password1.getText()).equals(String.valueOf(password2.getText()))) {
                user.updatePassword(String.valueOf(password1.getText()))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(MainActivity.TAG, "User password updated.");
                                    completeSaveChanges2();
                                } else {
                                    Log.d(MainActivity.TAG, "User password updating failed.");
                                    completeSaveChanges2();
                                }
                            }
                        });
            } else {
                Toast.makeText(FirebaseUIActivity.this, "Please enter the new password twice", Toast.LENGTH_SHORT).show();
            }
        }else{
            completeSaveChanges2();
        }
    }

    private  void completeSaveChanges2() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        TextView name = findViewById(R.id.name);
        TextView surname = findViewById(R.id.surname);
        // update display name
        if (!String.valueOf(name.getText()).isEmpty() && !String.valueOf(surname.getText()).isEmpty()) {
            String nameText;
            String surnameText;
            if (!String.valueOf(name.getText()).isEmpty()) {
                nameText = String.valueOf(name.getText());
            } else {
                nameText = user.getDisplayName().split(" ", 2)[0];
            }
            if (!String.valueOf(surname.getText()).isEmpty()) {
                surnameText = String.valueOf(surname.getText());
            } else {
                surnameText = user.getDisplayName().split(" ", 2)[1];
            }
            Log.v(MainActivity.TAG, "photoPath: " + photoPath);
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(nameText + " " + surnameText)
                    .setPhotoUri(Uri.parse(photoPath))

                    .build();

            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(MainActivity.TAG, "User profile updated.");
                                completeSaveChanges3();
                            } else {
                                Log.d(MainActivity.TAG, "User profile updating failed.");
                                completeSaveChanges3();
                            }
                        }
                    });

        }else{
            completeSaveChanges3();
        }
    }

    private void completeSaveChanges3() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        TextView email = findViewById(R.id.email);
        // update email address
        if (!String.valueOf(email.getText()).isEmpty()) {
            user.updateEmail(String.valueOf(email.getText()))
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(MainActivity.TAG, "User email address updated.");
                                saveChangesDone();
                            } else {
                                Log.d(MainActivity.TAG, "User email address updating failed.");
                                saveChangesDone();
                            }
                        }
                    });
        } else {
            saveChangesDone();
        }
    }

    private void saveChangesDone(){
        // turn off loading bar
        ProgressBar loading = findViewById(R.id.loadingProgressBar);
        loading.setVisibility(View.GONE);
        Toast.makeText(FirebaseUIActivity.this, "Changes saved!", Toast.LENGTH_SHORT).show();
        updateProfile();
    }

    private void reauthenticate(){
        logOut();
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                REAUTHENTICATE);
    }

    // method to delete an account
    public void delete() {
        // [START auth_fui_delete]
        AuthUI.getInstance()
                .delete(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
        // [END auth_fui_delete]
    }

    private void addProfileImageFirebaseDB() {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) ((ImageView) findViewById(R.id.profileImage)).getDrawable();
        if (bitmapDrawable == null) {
            Log.d(MainActivity.TAG, "No profile picture selected.");
            completeSaveChanges1();
            return;
        }
        Log.d(MainActivity.TAG, "Profile picture selected.");
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] data = baos.toByteArray();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference photoRef = storageRef.child("profileImages").child(user.getUid() + ".jpg");
        UploadTask uploadTask = photoRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.d(MainActivity.TAG, "Photo upload failed.");
            }
        }).addOnSuccessListener(new PhotoUploadSuccessListener());
    }

    private class PhotoUploadSuccessListener implements OnSuccessListener<UploadTask.TaskSnapshot> {
        @Override
        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            taskSnapshot.getMetadata().getReference().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(final Uri uri) {
                    photoPath = uri.toString();
                    Log.d(MainActivity.TAG,"Photo upload successful");
                    completeSaveChanges1();
                }
            });
        }
    }

    public void chooseImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void copyImage(Uri uriInput, File fileOutput) throws IOException {
        InputStream in = null;
        OutputStream out = null;

        try {
            in = getContentResolver().openInputStream(uriInput);
            out = new FileOutputStream(fileOutput);
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            in.close();
            out.close();
        }
    }

    public static Bitmap rotateImage(Bitmap bitmap, Uri photoUri) {
        try {
            File imageFile = new File(photoUri.getPath());
            int rotate = 0;
            // check orientation information on image
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            // create rotation matrix
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            int size = Math.min(bitmap.getWidth(), bitmap.getHeight());
            // rotate and crop image
            return Bitmap.createBitmap(bitmap, (bitmap.getWidth()-size)/2, (bitmap.getHeight()-size)/2, size,
                    size, matrix, true);
        } catch (IOException e) {
            Log.e(MainActivity.TAG, "Rotation failed: " + e.getMessage());
            return bitmap;
        }
    }

    public void getEmergencyContactFromDB(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        Log.d(MainActivity.TAG, "Starting database download of emergency contact with uid: " + user.getUid());
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        // get reference of the specified flight
        final DatabaseReference flightRef = database.getReference(user.getUid()).child("emergency_contact");
        flightRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flightRef.removeEventListener(this);
                String name = null;
                String number = null;
                // retrieve name and number
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    if(item != null && item.getValue() != null) {
                        Log.d(MainActivity.TAG, "Retrieving emergency contact " + item.getKey() + ": " + item.getValue());
                        if(item.getKey().equals("name")){
                            name = item.getValue().toString();
                        }else if(item.getKey().equals("number")){
                            number = item.getValue().toString();
                        }
                    }else{
                        Log.w(MainActivity.TAG, "Warning: No emergency contact found");
                    }
                }
                Log.d(MainActivity.TAG, "Database EC download complete");

                // put the method that processes the flight data data here!
                mProfile.ECName = name;
                mProfile.ECNumber = number;
                // --------------------------------------------------------
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(MainActivity.TAG, "Database EC download failed: " + databaseError.getMessage());
                Toast.makeText(MyApplication.getContext(),"Failed to get emergency contact info", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
