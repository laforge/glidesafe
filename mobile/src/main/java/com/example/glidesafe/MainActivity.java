/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYSeries;
import com.example.flightinfo.FullFlight;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //FOR DESIGN
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private boolean record = false;
    static final int START_FLIGHT = 1;
    public static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;

    public static final String TAG = "DEBUG_TAG";
    public static final String USER_ID = "USER_ID";
    public static final String SEND_SMS = "SEND_SMS";
    public static final int LOGIN_REQUEST = 2;
    public static final int SETTINGS_REQUEST = 3;
    private FirebaseAuth mAuth;
    private Context context;

    private Profile mProfile = new Profile();

    static ArrayList<FullFlight> allFlights;

    private class fetchItems extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params)
        {
            allFlights = FullFlight.getFlights(context);
            Log.v("debug_tag", "fetched!");
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 6 - Configure all views

        this.configureToolBar();

        this.configureDrawerLayout();

        this.configureNavigationView();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            updateProfile();
        }

        // Pre-loading for history
        this.context = this;
        new fetchItems().execute();

    }

    public void updateProfile(){
        Button loginButton = findViewById(R.id.loginButton);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        StorageReference storageRef;
        if(user.getPhotoUrl() != null) {
            mProfile.ProfileImagePath = user.getPhotoUrl().toString();
        }else{
            String DEFAULT_PROFILE_IMAGE = "https://firebasestorage.googleapis.com/v0/b/glidesafe-e7088.appspot.com/o/profileImages%2Fplaceholder.jpg?alt=media&token=afe2badf-4042-4a2f-9ea9-6fbf18516d23";
            mProfile.ProfileImagePath = DEFAULT_PROFILE_IMAGE;
        }

        getEmergencyContactFromDB();

        if(user == null){
            loginButton.setText(R.string.Log_In);
        }else{
            loginButton.setText(user.getDisplayName());
            mProfile.Name = user.getDisplayName();
            mProfile.UID = user.getUid();
            mProfile.Email = user.getEmail();
        }
    }

    public void startFlight(View v){
        Log.v("debug_tag", "Asking..");
        //handle permissions first, before map is created. not depicted here
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.v("debug_tag", "Permission not granted. Asking");
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Log.v("debug_tag","lala");
            } else {
                Log.v("debug_tag","lolo");

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

            }
        }

        TextView button = findViewById(R.id.button);
        button.setText("Flight has started !");

        startRecordingOnWear();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NotNull String[] permissions, @NotNull int[] grantResults) {
        // WE NEED TO TAKE CARE OF THE PERMISSIONS MESS
    }

    public void loginCallback(View view){
        // start FirebaseUIActivity
        Intent loginIntent = new Intent(this, FirebaseUIActivity.class);
        loginIntent.putExtra("Profile", mProfile);
        startActivityForResult(loginIntent, LOGIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGIN_REQUEST:
                Button loginButton = findViewById(R.id.loginButton);
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (resultCode == RESULT_OK) {
                    String status = data.getStringExtra("Status");
                    Log.d(TAG, "Got status result: " + status);
                    if (status.equals("logged_out") || user == null) {
                        loginButton.setText(R.string.Log_In);
                    } else {
                        loginButton.setText(user.getDisplayName());
                    }
                    mProfile = (Profile) data.getSerializableExtra("Profile");
                } else if (user != null) {
                    loginButton.setText(user.getDisplayName());
                }
                break;
            case SETTINGS_REQUEST:
                if (resultCode == RESULT_OK) {
                    mProfile = (Profile) data.getSerializableExtra("Profile");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // 5 - Handle back click to close menu
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // 4 - Handle Navigation Item Click
        int id = item.getItemId();

        switch (id){
            case R.id.activity_main_drawer_home :
                Intent homeIntent = new Intent(this, MainActivity.class);
                startActivity(homeIntent);
                break;
            case R.id.activity_main_drawer_history :
                Intent historyIntent = new Intent(this, History.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST",allFlights);
                historyIntent.putExtra("Bundle",args);
                startActivity(historyIntent);
                break;
            case R.id.activity_main_drawer_profile:
                // start FirebaseUIActivity
                Intent loginIntent = new Intent(this, FirebaseUIActivity.class);
                loginIntent.putExtra("Profile", mProfile);
                startActivityForResult(loginIntent, LOGIN_REQUEST);
                break;
            case R.id.activity_main_drawer_settings:
                Intent settingIntent = new Intent(this, Settings.class);
                settingIntent.putExtra("Profile", mProfile);
                startActivityForResult(settingIntent, SETTINGS_REQUEST);
                break;
            default:
                break;
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    // ---------------------
    // CONFIGURATION
    // ---------------------

    // 1 - Configure Toolbar
    private void configureToolBar(){
        this.toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
    }

    // 2 - Configure Drawer Layout
    private void configureDrawerLayout(){
        this.drawerLayout = findViewById(R.id.activity_main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    // 3 - Configure NavigationView
    private void configureNavigationView(){
        this.navigationView = findViewById(R.id.activity_main_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void getEmergencyContactFromDB(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        Log.d(MainActivity.TAG, "Starting database download of emergency contact with uid: " + user.getUid());
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        // get reference of the specified flight
        final DatabaseReference flightRef = database.getReference(user.getUid()).child("emergency_contact");
        flightRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flightRef.removeEventListener(this);
                String name = null;
                String number = null;
                // retrieve name and number
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    if(item != null && item.getValue() != null) {
                        Log.d(MainActivity.TAG, "Retrieving emergency contact " + item.getKey() + ": " + item.getValue());
                        if(item.getKey().equals("name")){
                            name = item.getValue().toString();
                        }else if(item.getKey().equals("number")){
                            number = item.getValue().toString();
                        }
                    }else{
                        Log.w(MainActivity.TAG, "Warning: No emergency contact found");
                    }
                }
                Log.d(MainActivity.TAG, "Database EC download complete");

                // put the method that processes the flight data data here!
                mProfile.ECName = name;
                mProfile.ECNumber = number;
                // --------------------------------------------------------
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(MainActivity.TAG, "Database EC download failed: " + databaseError.getMessage());
                Toast.makeText(MyApplication.getContext(),"Failed to get emergency contact info", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(new BroadcastReceiver() {
                     @Override
                     public void onReceive(Context context, Intent intent) {
                         sendSMS(intent.getStringExtra("LAT_LON"));
                     }
                 },
                new IntentFilter(SEND_SMS));
        }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void startRecordingOnWear() {
        Log.d(TAG, "Entered smart watch hr reading");
        Intent intentStartRec = new Intent(MainActivity.this, WearService.class);
        intentStartRec.setAction(WearService.ACTION_SEND.STARTACTIVITY.name());
        intentStartRec.putExtra(WearService.ACTIVITY_TO_START, BuildConfig.W_recordingactivity);
        MainActivity.this.startService(intentStartRec);
    }

    public void sendSMS(String lat_lon){
        if(mProfile.ECNumber != null) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(mProfile.ECNumber, null,
                    "GlideSafe SOS message. Last recorded position: " + lat_lon,
                    null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.", Toast.LENGTH_LONG).show();
        }
    }
}
