/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Settings extends AppCompatActivity {

    public static final String RECEIVE_HEART_RATE_LOCATION = "RECEIVE_HEART_RATE_LOCATION";
    public static final String HEART_RATE = "HEART_RATE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String LATITUDE = "LATITUDE";
    public static final String SPEED = "SPEED";
    public static final String ACC = "ACC";
    public static final String HEIGHT = "HEIGHT";
    private static final String TAG = "FLIGHTVIS";

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 3;
    private static final int RESULT_PICK_CONTACT = 4;
    
    private Profile mProfile = new Profile();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        TextView emergencyContactName = findViewById(R.id.emergencyContactName);

        // fetch user id
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mProfile = (Profile) getIntent().getSerializableExtra("Profile");
        if(mProfile.ECName == null){
            Log.w(MainActivity.TAG, "Settings: No EC found");
            emergencyContactName.setText(R.string.emergency_contact_not_selected);
        }else{
            emergencyContactName.setText(mProfile.ECName);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Emergency contact setup failed.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void selectEmergencyContact(View v){
        // check if sms permission is granted
        if (ContextCompat.checkSelfPermission(Settings.this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Settings.this,
                    new String[]{Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        }else {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    Cursor cursor = null;
                    try {
                        Uri uri = data.getData();
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        cursor.moveToFirst();
                        int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

                        uploadEmergencyContact(cursor.getString(nameIndex), cursor.getString(phoneIndex));

                        setECNameNumber(cursor.getString(nameIndex), cursor.getString(phoneIndex));

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    break;
            }
        } else {
            Log.e(MainActivity.TAG, "Failed to pick contact");
        }
    }

    public void uploadEmergencyContact(final String name, final String number){
        // fetch user id
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            Log.e(MainActivity.TAG, "Error: User is null");
            return;
        }
        Log.d(MainActivity.TAG, "Starting database upload with uid: " + user.getUid());
        // get reference of DB of user
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference(user.getUid());
        userRef.runTransaction(new Transaction.Handler(){
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                // add new data to DB
                mutableData.child("emergency_contact").child("name").setValue(name);
                mutableData.child("emergency_contact").child("number").setValue(number);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (b) {
                    Log.d(MainActivity.TAG, "Database upload successful");
                } else {
                    Log.d(MainActivity.TAG, "Database upload failed: " + databaseError.getMessage());
                }
            }
        });
    }
    
    public void setECNameNumber(String name, String number){
        mProfile.ECName = name;
        mProfile.ECNumber = number;
        TextView ECname = findViewById(R.id.emergencyContactName);
        if(name != null && number != null) {
            Log.v(MainActivity.TAG, "Name and Contact number is: " + name + ", " + number);
            ECname.setText(name);
        }else{
            ECname.setText(R.string.emergency_contact_not_selected);
        }
        Intent resultIntent = new Intent();
        resultIntent.putExtra("Profile", mProfile);
        setResult(RESULT_OK, resultIntent);
    }
}
