/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.flightinfo.FullFlight;

import java.util.ArrayList;
import java.util.Locale;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private ArrayList<FullFlight> mDataset;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(FullFlight item);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public TextView date;
        public TextView duration;
        public TextView takeOff;
        public TextView landing;

        public MyViewHolder(View v) {
            super(v);
            this.itemView = v;
            this.date = v.findViewById(R.id.item_date);
            this.duration = v.findViewById(R.id.item_duration);
            this.takeOff = v.findViewById(R.id.item_takeOff);
            this.landing = v.findViewById(R.id.item_landing);
        }

        public void bind(final FullFlight item, final OnItemClickListener listener) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            String[] data_tmp = item.getFlightDate().toString().split(" ", 6);
            date.setText("Flight from: " + data_tmp[1] + " " + data_tmp[2] + " " + data_tmp[5] + ", " + data_tmp[3]);
            duration.setText("Flight duration: " + String.format(Locale.ENGLISH,"%d:%d",
                    item.getFlightDuration().toHours(),
                    item.getFlightDuration().toMinutes()));
            takeOff.setText("Take off in: " + item.getTakeOffSite());
            //Log.v("debug_tag", mDataset.get(position).getTakeOffSite());
            landing.setText("Landing in: " + item.getLandingSite());

            itemView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoryAdapter(ArrayList<FullFlight> myDataset, OnItemClickListener listener) {
        this.listener = listener;

        mDataset = myDataset;

        // sort flight list
        for(int i = 0; i < mDataset.size(); i++){
            for(int j = 0; j < mDataset.size() - 1 - i; j++){
                if(mDataset.get(j).getFlightDate().compareTo(mDataset.get(j + 1).getFlightDate()) < 0){
                    FullFlight tmp = mDataset.get(j);
                    mDataset.set(j, mDataset.get(j + 1));
                    mDataset.set(j + 1, tmp);
                }
            }
        }
    }



    // Create new views (invoked by the layout manager)
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_myhistory_layout, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(mDataset.get(position), listener);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}


