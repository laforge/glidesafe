/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.flightinfo;

import android.location.Location;

public class OsmNode {
    private double lat;
    private double lon;
    private String name;
    private int id;

    OsmNode(int id, String name, double lat, double lon){
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.name = name;
    }


    double distanceTo(double lat, double lon){
        float [] results = new float[5];

        Location.distanceBetween(lat,lon,this.lat, this.lon,results);
        return (double) results[0];
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
