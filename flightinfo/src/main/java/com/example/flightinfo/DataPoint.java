/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.flightinfo;

import org.osmdroid.util.GeoPoint;

import java.io.Serializable;
import java.time.Instant;

public class DataPoint implements Serializable {
    private static final long serialVersionUID = 4203614310606692185L;

    public GeoPoint geoPoint;
    public Instant instant;
    public double acc; // Scalar acceleration. From accelerometer
    public double speed; // Scalar speed. Derived from geoPoint.
    public double verticalSpeed; // Scalar vertical speed. Derived from pressure sensor

    public DataPoint(Instant instant, GeoPoint geoPoint, double verticalSpeed, double speed, double acc){
        this.instant = instant;
        this.geoPoint = geoPoint;
        this.verticalSpeed = verticalSpeed;
        this.speed = speed;
        this.acc = acc;
    }
}
