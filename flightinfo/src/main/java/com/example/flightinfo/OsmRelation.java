/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.flightinfo;

import java.util.ArrayList;

public class OsmRelation {
    ArrayList<OsmWay> polys = new ArrayList<>();
    ArrayList<Integer> ids = new ArrayList<>();

    private int id;
    private String name;

    OsmRelation(int id, String name){
        this.id = id;
        this.name = name;
    }
    public void addWay(OsmWay osmWay){
        polys.add(osmWay);
    }

    //Shortest distance from one point to poly
    double distanceTo(double lat, double lon){
        double shortestDistance = Double.POSITIVE_INFINITY;

        for(OsmWay poly : polys){
            if(poly.distanceTo(lat,lon) < shortestDistance){
                shortestDistance = poly.distanceTo(lat,lon);
            }
        }

        return shortestDistance;
    }

    public String getName() {
        return name;
    }
}
