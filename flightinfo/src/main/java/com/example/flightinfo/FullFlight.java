/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.flightinfo;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FullFlight implements Serializable {
    private static final long serialVersionUID = 4203544510606692195L;
    private String id = UUID.randomUUID().toString();

    public ArrayList<DataPoint> flightPoints;
    private Duration flightDuration;
    private String landingSite = UNKNOWN;
    private String takeOffSite = UNKNOWN;
    private String user;
    private static Context context;
    private Date flightDate;

    static final String UNKNOWN = "Unknown";

    public void endFlight(RunnableFlight run){

        flightDuration = Duration.between(flightPoints.get(0).instant,
                flightPoints.get(flightPoints.size()-1).instant);
        new GetLocationsAndWrite().execute(new FullFlightRunWrapper(this, run));
    }


    private class FullFlightRunWrapper{
        private RunnableFlight run;
        private FullFlight flight;

        FullFlightRunWrapper(FullFlight flight, RunnableFlight run){
            this.run = run;
            this.flight = flight;
        }

        public RunnableFlight getRun() {
            return run;
        }

        public FullFlight getFlight() {
            return flight;
        }
    }

    public FullFlight(){
        this.flightPoints = new ArrayList<>();
        this.flightDate = new Date();

        if(user == null){
            user = "local";
        }
    }

    public void setContext(Context context){
        FullFlight.context = context;
    }

    public void setUser(String user){
        if(user == null){
            user = "local";
        }
        this.user = user;
    }

    public String getLandingSite() {
        return landingSite;
    }

    private void setLandingSite(String landingSite) {
        this.landingSite = landingSite;
    }

    public String getTakeOffSite() {
        return takeOffSite;
    }

    private void setTakeOffSite(String takeOffSite) {
        this.takeOffSite = takeOffSite;
    }

    Uri writeFlight(){
        Log.v("debug_tag", "Writing flight");
        ContextWrapper c = new ContextWrapper(context);
        String FileDirectory =  c.getFilesDir().getPath();
        File flightRootFolder = new File(FileDirectory, "Flight");

        if (!flightRootFolder.exists()) {
            if(!flightRootFolder.mkdir()){
                return null;
            }
        } else if (!flightRootFolder.isDirectory()) {
            Log.e("SafeGlide", "FlightRootFolder exist but is not a folder. Not saving flight");
            return null;
        }

        File flightSaveFolder = new File(flightRootFolder, user);
        if (!flightSaveFolder.exists()) {
            if(flightSaveFolder.mkdir()){
                return null;
            }
        } else if (!flightSaveFolder.isDirectory()){
            Log.e("SafeGlide", "FlightSaveFolder exist but is not a folder. Not saving flight");
            return null;
        }

        File fileToWrite = new File(flightSaveFolder, this.getId());

        try {
            FileOutputStream fileOut = new FileOutputStream(fileToWrite);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(this);
            objectOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.fromFile(fileToWrite);
    }

    public static ArrayList<FullFlight> getFlights(Context context){
        ArrayList<FullFlight> allFlights = new ArrayList<>();

        ContextWrapper c = new ContextWrapper(context);
        Path flightRoot =  (new File(c.getFilesDir(),"Flight")).toPath();

        try {
            Files.walk(flightRoot)
                    .filter(Files::isRegularFile)
                    .forEach(f -> {
                        try {
                            FileInputStream fis = new FileInputStream(new File(f.toString()));

                            ObjectInputStream is = new ObjectInputStream(fis);
                            allFlights.add((FullFlight)is.readObject());
                            is.close();
                            fis.close();
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allFlights;
    }

    public String getId() {
        return id;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public Duration getFlightDuration() {
        return flightDuration;
    }

    private static class GetLocationsAndWrite extends AsyncTask<FullFlightRunWrapper, String, String> {
        // Computation of bounding box inspired from:
        // https://stackoverflow.com/questions/238260/how-to-calculate-the-bounding-box-for-a-given-lat-lng-location
        private double WGS84EarthRadius(double lat){
            double WGS84_a = 6378137.0;
            double WGS84_b = 6356752.3;
            double An = WGS84_a*WGS84_a * Math.cos(lat);
            double Bn = WGS84_b*WGS84_b * Math.sin(lat);
            double Ad = WGS84_a * Math.cos(lat);
            double Bd = WGS84_b * Math.sin(lat);
            return Math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) );
        }

        private String getBoundingBox(double lat, double lon){
            int boundingBoxSizeM = 1000;
            double latRad = Math.PI*lat/180;
            double lonRad = Math.PI*lon/180;

            double halfSide = (double)boundingBoxSizeM;

            double radius = WGS84EarthRadius(latRad);
            double pradius = radius *Math.cos(latRad);

            double latMin = 180.0*(latRad - halfSide/radius)/Math.PI;
            double latMax = 180.0*(latRad + halfSide/radius)/Math.PI;
            double lonMin = 180.0*(lonRad - halfSide/pradius)/Math.PI;
            double lonMax = 180.0*(lonRad + halfSide/pradius)/Math.PI;

            return latMin+","+lonMin+","+latMax+","+lonMax;
        }

        private String post(String json) throws IOException {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

            MediaType JSON = MediaType.get("application/x-www-form-urlencoded");

            RequestBody body = RequestBody.create(json, JSON);
            Request request = new Request.Builder()
                    .url("http://overpass-api.de/api/interpreter")
                    .post(body)

                    .build();
            try (Response response = client.newCall(request).execute()){
                return Objects.requireNonNull(response.body()).string();
            }
        }

        private JSONArray queryOverpass(double lat, double lon, String tagToMatch){

            // Approximate formula for meters to long/lat
            // latitude degrees to add to localization

            String boundingBoxString = getBoundingBox(lat,lon);

            String requestString = "[out:json][timeout:25000];\n" +
                    "(node[" + tagToMatch +"]" +
                    "("+boundingBoxString+");\n" +
                    "way[" + tagToMatch + "]" +
                    "("+boundingBoxString+");\n" +
                    "relation[" + tagToMatch + "]" +
                    "("+boundingBoxString+");\n" +
                    ");\n" +
                    "out body;\n" +
                    ">;\n" +
                    "out skel qt;";

            String response;

            try {
                response = post(requestString);

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            System.out.println(response);

            JSONArray jResponse = null;
            try {
                jResponse = new JSONObject(response).getJSONArray("elements");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jResponse;
        }

        private void parseJToObj(JSONArray jArray, ArrayList<OsmNode> takeOffNode, ArrayList<OsmWay> takeOffWay,
                                 ArrayList<OsmRelation> takeOffRelation) throws JSONException{
            for(int i = 0; i < jArray.length(); i++){
                JSONObject currentElem = jArray.getJSONObject(i);

                // If it is a node ---------------------------------------------------------------
                if(currentElem.getString("type").equals("node")){
                    String name = "Undefined";
                    if(currentElem.has("tags")){
                        name = currentElem.getJSONObject("tags").optString("name", "Undefined");
                        Log.v("debug_tag_name", name);
                    }
                    OsmNode currentNode = new OsmNode(currentElem.getInt("id"),
                            name,
                            currentElem.getDouble("lat"),
                            currentElem.getDouble("lon"));

                    int nodeId = currentElem.getInt("id");
                    // Look through all ways
                    OsmWay mother = null;
                    for(OsmWay osmWay : takeOffWay){
                        if(osmWay.hasID(nodeId)){
                            mother = osmWay;
                            break;
                        }
                    }

                    if(mother != null){
                        mother.poly.add(currentNode);
                    } else {
                        takeOffNode.add(currentNode);
                    }
                    // If it is a way ---------------------------------------------------------
                } else if (currentElem.getString("type").equals("way")){
                    String name = "Undefined";
                    if(currentElem.has("tags")){
                        name = currentElem.getJSONObject("tags").optString("name", "Undefined");
                        Log.v("debug_tag_name", name);
                    }

                    OsmWay currentWay = new OsmWay(currentElem.getInt("id"), name);

                    for(int j = 0; j < currentElem.getJSONArray("nodes").length(); j++){
                        currentWay.ids.add(currentElem.getJSONArray("nodes").getInt(j));
                    }

                    // Add nodes that were wrongfully added to takeOffNode
                    for(int j = 0; j < takeOffNode.size(); j++) {
                        if (currentWay.ids.contains(takeOffNode.get(j).getId())) {
                            OsmNode temp = takeOffNode.get(j);
                            takeOffNode.remove(j--);
                            currentWay.poly.add(temp);
                        }
                    }

                    int wayId = currentElem.getInt("id");

                    // Does it have a parent?
                    OsmRelation mother = null;
                    for(OsmRelation osmRelation : takeOffRelation){
                        if(osmRelation.ids.contains(wayId)){
                            mother = osmRelation;
                            break;
                        }
                    }

                    if(mother == null){
                        takeOffWay.add(currentWay);
                    } else {
                        mother.polys.add(currentWay);
                    }
                    // If relation ----------------------------------------------------------------
                } else if (currentElem.getString("type").equals("relation")){
                    String name = "Undefined";
                    if(currentElem.has("tags")){
                        name = currentElem.getJSONObject("tags").optString("name", "Undefined");
                        Log.v("debug_tag_name", name);
                    }
                    OsmRelation currentRel = new OsmRelation(currentElem.getInt("id"), name);

                    for(int j = 0; j < currentElem.getJSONArray("members").length(); j++){
                        if(currentElem.getJSONArray("members").getJSONObject(j).
                                getString("type").equals("way")) {
                            currentRel.ids.add(currentElem.getJSONArray("members").
                                    getJSONObject(j).getInt("ref"));
                        }
                    }
                    // Add ways wrongfully added to takeOffWay
                    for(int j = 0; j < takeOffWay.size(); j++) {
                        if (currentRel.ids.contains(takeOffWay.get(j).getId())) {
                            OsmWay temp = takeOffWay.get(j);
                            takeOffWay.remove(j--);
                            currentRel.polys.add(temp);
                        }
                    }
                }
            }
        }

        private String findClosestName(double lat, double lon, ArrayList<OsmNode> takeOffNode,  ArrayList<OsmWay> takeOffWay,
                                       ArrayList<OsmRelation> takeOffRelation){
            String closestName = "Undefined";
            double closestDistance = Double.POSITIVE_INFINITY;

            for(OsmNode osmNode : takeOffNode){
                if(osmNode.distanceTo(lat, lon)< closestDistance){
                    closestDistance = osmNode.distanceTo(lat,lon);
                    closestName = osmNode.getName();
                }
            }

            for(OsmWay osmWay : takeOffWay){
                if(osmWay.distanceTo(lat,lon) < closestDistance){
                    closestDistance = osmWay.distanceTo(lat,lon);
                    closestName = osmWay.getName();
                }
            }

            for(OsmRelation osmRelation : takeOffRelation){
                if(osmRelation.distanceTo(lat,lon) < closestDistance){
                    closestDistance = osmRelation.distanceTo(lat,lon);
                    closestName = osmRelation.getName();
                }
            }

            return closestName;
        }


        @Override
        protected String doInBackground(FullFlightRunWrapper... runWrapper) {
            FullFlight currentFlight = runWrapper[0].getFlight();


            if(currentFlight.getTakeOffSite().equals(FullFlight.UNKNOWN)){
                double lat = currentFlight.flightPoints.get(0).geoPoint.getLatitude();
                double lon = currentFlight.flightPoints.get(0).geoPoint.getLongitude();

                JSONArray response = queryOverpass(lat,lon, "\"free_flying:site\"=\"takeoff\"");

                if(response != null) {
                    ArrayList<OsmNode> takeOffNode = new ArrayList<>();
                    ArrayList<OsmWay> takeOffWay = new ArrayList<>();
                    ArrayList<OsmRelation> takeOffRelation = new ArrayList<>();

                    try {
                        parseJToObj(response, takeOffNode, takeOffWay, takeOffRelation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }

                    String closestName = findClosestName(lat, lon, takeOffNode, takeOffWay, takeOffRelation);

                    currentFlight.setTakeOffSite(closestName);
                }
            }
            if(currentFlight.getLandingSite().equals(FullFlight.UNKNOWN)) {
                double lat = currentFlight.flightPoints.get(currentFlight.flightPoints.size() - 1).geoPoint.getLatitude();
                double lon = currentFlight.flightPoints.get(currentFlight.flightPoints.size() - 1).geoPoint.getLongitude();

                JSONArray response = queryOverpass(lat, lon, "\"free_flying:site\"=\"landing\"");

                if (response != null) {
                    ArrayList<OsmNode> takeOffNode = new ArrayList<>();
                    ArrayList<OsmWay> takeOffWay = new ArrayList<>();
                    ArrayList<OsmRelation> takeOffRelation = new ArrayList<>();

                    try {
                        parseJToObj(response, takeOffNode, takeOffWay, takeOffRelation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }

                    String closestName = findClosestName(lat, lon, takeOffNode, takeOffWay, takeOffRelation);

                    currentFlight.setLandingSite(closestName);

                }
            }

            Uri uri  = currentFlight.writeFlight();

            runWrapper[0].getRun().setUri(uri);

            (new Thread(runWrapper[0].getRun())).start();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
