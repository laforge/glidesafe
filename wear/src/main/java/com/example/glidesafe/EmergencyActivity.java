/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import static android.media.AudioAttributes.USAGE_ALARM;

public class EmergencyActivity extends WearableActivity {

    private AudioManager mAudioManager = null;
    private int origionalVolume;
    private MediaPlayer mediaPlayer = null;
    private Vibrator vibrator = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        String lat_lon = getIntent().getStringExtra("LAT_LON");

        // Enables Always-on
        setAmbientEnabled();

        if(checkSpeaker()){
            // sett volume to max even if in silent mode (this is an emergency)
            mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            origionalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            // play alarm sound
            mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
            mediaPlayer.setLooping(true);
            mediaPlayer.setVolume(1,1);
            mediaPlayer.start();

        }
        // start vibrating watch for 30 sec
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(30000);

        // start a timer for 30 sec - when finished, send emergency sms
        new CountDownTimer(30000, 100) {
            public void onTick(long millisUntilFinished) {
                TextView countDownDisplay = findViewById(R.id.countDownDisplay);
                countDownDisplay.setText(millisUntilFinished / 1000 + "." + millisUntilFinished % 1000 / 100);
            }

            public void onFinish() {
                TextView countDownDisplay = findViewById(R.id.countDownDisplay);
                countDownDisplay.setText("SMS sent!");
                sendSMS(lat_lon);
            }
        }.start();
    }

    private boolean checkSpeaker(){
        PackageManager packageManager = this.getPackageManager();
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // Check whether the device has a speaker.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                // Check FEATURE_AUDIO_OUTPUT to guard against false positives.
                packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_OUTPUT)) {
            AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
            for (AudioDeviceInfo device : devices) {
                if (device.getType() == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
                    return true;
                }
            }
        }
        return false;
    }

    public void okCallback(View v){
        Intent intentEndFlight = new Intent(EmergencyActivity.this, WearService.class);
        intentEndFlight.setAction(WearService.ACTION_SEND.END_FLIGHT.name());
        startService(intentEndFlight);

        // turning off alarm and vibration
        if(mediaPlayer != null) {
            // restore original volume
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, origionalVolume, 0);
            // stop playing alarm sound
            mediaPlayer.release();
            mediaPlayer = null;
        }
        // stop vibration
        vibrator.cancel();
        vibrator = null;

        Intent finalIntent = new Intent(EmergencyActivity.this, FlightOver.class);
        startActivity(finalIntent);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(mediaPlayer != null) {
            // restore original volume
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, origionalVolume, 0);
            // stop playing alarm sound
            mediaPlayer.release();
            mediaPlayer = null;
        }
        // stop vibration
        vibrator.cancel();
        vibrator = null;
    }

    private void sendSMS(String lat_lon){
        Intent intentSendSMS = new Intent(EmergencyActivity.this, WearService.class);
        intentSendSMS.setAction(WearService.ACTION_SEND.MESSAGE.name());
        intentSendSMS.putExtra(WearService.PATH, BuildConfig.W_path_send_sms);
        intentSendSMS.putExtra(WearService.MESSAGE, lat_lon);
        startService(intentSendSMS);
    }
}
