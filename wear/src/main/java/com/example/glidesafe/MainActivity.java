/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.time.Instant;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.flightinfo.DataPoint;
import com.example.flightinfo.FullFlight;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import org.osmdroid.util.GeoPoint;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.example.glidesafe.WearService.HEART_RATE;
import static com.example.glidesafe.WearService.LATITUDE;
import static com.example.glidesafe.WearService.LONGITUDE;

public class MainActivity extends WearableActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mPressure;
    private Sensor mAcc;
    private Sensor mHeart;

    static boolean activeHandler = false;

    public static final String STOP_ACTIVITY = "STOP_ACTIVITY";
    private GlideSafeRoomDatabase GlideSafeDB;

    private List<Integer> hrList = new ArrayList<>();
    private List<Integer> hrListCopy = new ArrayList<>();
    private List<Integer> heightList = new ArrayList<>();
    private List<Integer> heightListCopy = new ArrayList<>();
    private List<Integer> speedList = new ArrayList<>();
    private List<Integer> speedListCopy = new ArrayList<>();
    private List<Integer> accList = new ArrayList<>();
    private List<Integer> accListCopy = new ArrayList<>();
    static ArrayList<Integer> hrArray;
    static ArrayList<Integer> speedArray;
    static ArrayList<Integer> accArray;
    static ArrayList<Integer> heightArray;
    private List<Location> locationList = new ArrayList<Location>();
    private List<Location> locationListCopy = new ArrayList<Location>();
    static float[] latitudeArray;
    static float[] longitudeArray;
    private int sizeListToSave = 10;

    private float acceleration;
    private int height;
    private boolean firstPosition = true;

    private double firstLatitude, firstLongitude;
    private Location firstLocation = new Location("point A");

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private ConstraintLayout mLayout;
    private double[] lastspeeds = new double[20];
    private int speedIndex = 0;

    static FullFlight currentFlight;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < 20; i++){
            lastspeeds[i] = 0;
        }

        currentFlight = new FullFlight();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mHeart = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);

        if (checkSelfPermission("android.permission.BODY_SENSORS") == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{"android.permission.BODY_SENSORS"}, 0);
        }

        if (checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == PackageManager.PERMISSION_DENIED || checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_DENIED || checkSelfPermission("android.permission.INTERNET") == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.INTERNET"}, 0);
        }

        // Create instance of Sport Tracker Room DB
        GlideSafeDB = GlideSafeRoomDatabase.getDatabase(getApplicationContext());

        fusedLocationClient = new FusedLocationProviderClient(this);

        TextView textViewSpeed= findViewById(R.id.textView4);
        textViewSpeed.setText("Speed : 0");
        TextView tv3 = findViewById(R.id.textView3);
        tv3.setText("Heart rate : 0");

        // Location callback
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                TextView textViewSpeed = findViewById(R.id.textView4);

                if (firstPosition) {
                    firstLatitude = locationResult.getLastLocation().getLatitude();
                    firstLongitude = locationResult.getLastLocation().getLongitude();
                    firstLocation.setLatitude(firstLatitude);
                    firstLocation.setLongitude(firstLongitude);
                    firstPosition = false;
                }

                double latitude = locationResult.getLastLocation().getLatitude();
                double longitude = locationResult.getLastLocation().getLongitude();
                double speed = locationResult.getLastLocation().getSpeed();

                currentFlight.flightPoints.add(new DataPoint(Instant.now(),
                        new GeoPoint(latitude, longitude, height), 0, speed, acceleration));

                Location currentLocation = new Location("point B");

                currentLocation.setLatitude(latitude);
                currentLocation.setLongitude(longitude);

                lastspeeds[speedIndex] = speed;
                speedIndex = (speedIndex+1) % 20;

                double meanSpeed = 0;
                for (int i = 0; i < 20; i++) {
                    meanSpeed += lastspeeds[i];
                }

                meanSpeed = meanSpeed / 20.0;

                float distance = firstLocation.distanceTo(currentLocation);
                if (distance >= 50 && meanSpeed <= 0.5) {
                    Intent emergencyIntent = new Intent(MainActivity.this, EmergencyActivity.class);
                    emergencyIntent.putExtra("LAT_LON", "lat: " + String.valueOf(latitude) + ", lon: " + String.valueOf(longitude));
                    startActivity(emergencyIntent);
                    activeHandler = false;
                }

                double kmspeed = speed *3.6;
                textViewSpeed.setText(String.format("Speed : %.2f", kmspeed));

/*
                speedList.add(intspeed);
                if (!speedList.isEmpty()) {
                    speedListCopy.clear();
                    speedListCopy.addAll(speedList);
                    SavingSpeedAsyncTask speedAsyncTask = new SavingSpeedAsyncTask(GlideSafeDB);
                    speedAsyncTask.execute(speedListCopy);
                    speedList.clear();
                }

                // Save the data in case the tablet is not in range:
                // when the activity is over, we send the whole sensor database back
                locationList.add(locationResult.getLastLocation());
                if (!locationList.isEmpty()) {
                    locationListCopy.clear();
                    locationListCopy.addAll(locationList);
                    SavingLocationAsyncTask locationAsyncTask = new SavingLocationAsyncTask(GlideSafeDB);
                    locationAsyncTask.execute(locationListCopy);
                    locationList.clear();
                }*/
            }
        };


/*        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mSensorManager.unregisterListener(MainActivity.this);
                //Read HR and Location data
                OnTaskCompletedListener onTaskCompletedListener = new OnTaskCompletedListener() {
                    @Override
                    public void onTaskCompleted() {
                        //Send HR data and Location in the same intent
                        Intent intentSendHrLocation = new Intent(MainActivity.this, WearService.class);
                        intentSendHrLocation.setAction(WearService.ACTION_SEND.HEART_RATE_AND_LOCATION.name());
                        intentSendHrLocation.putIntegerArrayListExtra(HEART_RATE, hrArray);
                        intentSendHrLocation.putExtra(LONGITUDE, longitudeArray);
                        intentSendHrLocation.putExtra(LATITUDE, latitudeArray);
                        intentSendHrLocation.putIntegerArrayListExtra(WearService.HEIGHT, heightArray);
                        startService(intentSendHrLocation);
                        finish();
                    }
                };

                ReadingHeartRateAndLocationAsyncTask hrAsyncTask = new ReadingHeartRateAndLocationAsyncTask(onTaskCompletedListener, GlideSafeDB);
                hrAsyncTask.execute();
            }
        }, new IntentFilter(STOP_ACTIVITY));*/

        // Enables Always-on
        setAmbientEnabled();

    }

    @Override
    public final void onSensorChanged(SensorEvent event){

        TextView tv1 = findViewById(R.id.textView);
        TextView tv2 = findViewById(R.id.textView2);
        TextView tv3 = findViewById(R.id.textView3);
        float accx, accy, accz;
        float pressure, g;
        int hr;
        int h;

        if(event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            pressure = event.values[0];
            h = (int) ((int)((Math.pow((1013.25/pressure),0.19022)-1)*298.15)/0.0065);
            String display = "Height :" + h;
            height = h;
            tv1.setText(display);

/*            heightList.add(h);
            if (!heightList.isEmpty()) {
                heightListCopy.clear();
                heightListCopy.addAll(heightList);
                SavingHeightAsyncTask heightAsyncTask = new SavingHeightAsyncTask(GlideSafeDB);
                heightAsyncTask.execute(heightListCopy);
                heightList.clear();
            }*/

/*            // Send the data for live update on the tablet
            Intent intent = new Intent(MainActivity.this, WearService.class);
            intent.setAction(WearService.ACTION_SEND.PRESSURE.name());
            intent.putExtra(WearService.PRESSURE, pressure);
            startService(intent);*/
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            accx = event.values[0]; accy = event.values[1]; accz = event.values[2];
            g = (float) (Math.sqrt(accx*accx+accy*accy+accz*accz)/9.81);
            String display = "g : " + g;
            acceleration = g;
            tv2.setText(String.format("g : %.2f", g));
        }

        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE){
            hr = (int) event.values[0];
            tv3.setText("Heart rate : " + hr);

            // Save the data in case the tablet is not in range:
            // when the activity is over, we send the whole sensor database back
/*
            hrList.add(hr);
            if (!hrList.isEmpty()) {
                hrListCopy.clear();
                hrListCopy.addAll(hrList);
                SavingHeartRateAsyncTask hrAsyncTask = new SavingHeartRateAsyncTask(GlideSafeDB);
                hrAsyncTask.execute(hrListCopy);
                hrList.clear();
            }
*/

/*            // Send the data for live update on the tablet
            Intent intent = new Intent(MainActivity.this, WearService.class);
            intent.setAction(WearService.ACTION_SEND.HEART_RATE.name());
            intent.putExtra(WearService.HEART_RATE, hr);
            startService(intent);*/
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mHeart, SensorManager.SENSOR_DELAY_UI);
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        stopLocationUpdates();
    }

    private void startLocationUpdates() {
        LocationRequest locationRequest = new LocationRequest().setInterval(5).setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

/*    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        updateDisplay();
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();
        updateDisplay();
    }

    private void updateDisplay() {
        if (isAmbient()) {
            mLayout.setBackgroundColor(getResources().getColor(android.R.color.black, getTheme()));
        } else {
            mLayout.setBackgroundColor(getResources().getColor(android.R.color.white, getTheme()));
        }
    }*/

}
