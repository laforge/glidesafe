/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.location.Location;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class SavingLocationAsyncTask extends AsyncTask<List<Location>, Void, Void> {

    private GlideSafeRoomDatabase db;

    SavingLocationAsyncTask(GlideSafeRoomDatabase db) {
        this.db = db;
    }

    @SafeVarargs

    @Override
    protected final Void doInBackground(List<Location>... lists) {
        List<Location> locationValueList = lists[0];
        List<SensorDataEntity> sensorDataEntityList = new ArrayList<SensorDataEntity>();

        for (Location locationValue : locationValueList) {
            long time = System.nanoTime();

            // Send Latitude
            SensorDataEntity sensorDataLat = new SensorDataEntity();
            sensorDataLat.timestamp = time;
            sensorDataLat.type = SensorDataEntity.LATITUDE;
            sensorDataLat.value = locationValue.getLatitude();
            sensorDataEntityList.add(sensorDataLat);

            // Send Longitude
            SensorDataEntity sensorDataLon = new SensorDataEntity();
            sensorDataLon.timestamp = time;
            sensorDataLon.type = SensorDataEntity.LONGITUDE;
            sensorDataLon.value = locationValue.getLongitude();
            sensorDataEntityList.add(sensorDataLon);
        }
        db.sensorDataDao().insertSensorDataEntityList(sensorDataEntityList);
        return null;
    }
}
