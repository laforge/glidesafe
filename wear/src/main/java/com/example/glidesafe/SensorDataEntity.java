/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Annotation for Room library to recognize the entity
@Entity
public class SensorDataEntity {
    //Different types of sensors
    public final static int HEART_RATE = 0;
    public final static int LATITUDE = 1;
    public final static int LONGITUDE = 2;
    public final static int ACC = 3;
    public final static int SPEED = 4;
    public final static int HEIGHT = 5;

    //Primary key to access the row of SQLite table for the entity SensorData
    @PrimaryKey(autoGenerate = true)
    public int uid;
    //Different coloumn for different attributes of the entity SensorData
    @ColumnInfo
    public long timestamp;
    @ColumnInfo
    public int type;
    @ColumnInfo
    public double value;
}
