/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class SavingHeartRateAsyncTask extends AsyncTask<List<Integer>, Void, Void> {

    private GlideSafeRoomDatabase db;

    SavingHeartRateAsyncTask(GlideSafeRoomDatabase db) {
        this.db = db;
    }


    @SafeVarargs
    @Override
    protected final Void doInBackground(List<Integer>... lists) {
        List<Integer> hrValueList = lists[0];
        List<SensorDataEntity> sensorDataEntityList = new ArrayList<SensorDataEntity>();
        for (Integer hrValue : hrValueList) {
            SensorDataEntity sensorData = new SensorDataEntity();
            sensorData.timestamp = System.nanoTime();
            sensorData.type = SensorDataEntity.HEART_RATE;
            sensorData.value = hrValue;
            sensorDataEntityList.add(sensorData);
        }
        db.sensorDataDao().insertSensorDataEntityList(sensorDataEntityList);
        return null;
    }
}
