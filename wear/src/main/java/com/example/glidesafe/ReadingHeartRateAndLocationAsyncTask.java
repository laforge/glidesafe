/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class ReadingHeartRateAndLocationAsyncTask extends AsyncTask<Void, Void, List<SensorDataEntity>> {

    private final GlideSafeRoomDatabase db;
    private final OnTaskCompletedListener onTaskCompletedListener;

    ReadingHeartRateAndLocationAsyncTask(OnTaskCompletedListener onTaskCompletedListener, GlideSafeRoomDatabase db) {
        this.onTaskCompletedListener = onTaskCompletedListener;
        this.db = db;
    }


    @Override
    protected List<SensorDataEntity> doInBackground(Void... voids) {
        List<SensorDataEntity> sensorDataEntityList = db.sensorDataDao().getAllValues(SensorDataEntity.HEART_RATE);
        sensorDataEntityList.addAll(db.sensorDataDao().getAllValues(SensorDataEntity.LONGITUDE));
        sensorDataEntityList.addAll(db.sensorDataDao().getAllValues(SensorDataEntity.LATITUDE));
        sensorDataEntityList.addAll(db.sensorDataDao().getAllValues(SensorDataEntity.SPEED));
        sensorDataEntityList.addAll(db.sensorDataDao().getAllValues(SensorDataEntity.HEIGHT));
        sensorDataEntityList.addAll(db.sensorDataDao().getAllValues(SensorDataEntity.ACC));
        db.sensorDataDao().deleteAll();
        return sensorDataEntityList;
    }

    @Override
    protected void onPostExecute(List<SensorDataEntity> dbValues) {
        super.onPostExecute(dbValues);

        //Filter only HR values
        List<SensorDataEntity> hrValues = new ArrayList<SensorDataEntity>();
        hrValues.addAll(dbValues);
        hrValues.removeIf(p -> p.type != SensorDataEntity.HEART_RATE);

        MainActivity.hrArray = new ArrayList();
        for (int i = 0; i < hrValues.size(); i++) {
            MainActivity.hrArray.add((int) dbValues.get(i).value);
        }

        //Filter only Height values
        List<SensorDataEntity> heightValues = new ArrayList<SensorDataEntity>();
        heightValues.addAll(dbValues);
        heightValues.removeIf(p -> p.type != SensorDataEntity.HEIGHT);

        MainActivity.heightArray = new ArrayList();
        for (int i = 0; i < heightValues.size(); i++) {
            MainActivity.heightArray.add((int) dbValues.get(i).value);
        }

        //Filter only Speed values
        List<SensorDataEntity> speedValues = new ArrayList<SensorDataEntity>();
        speedValues.addAll(dbValues);
        speedValues.removeIf(p -> p.type != SensorDataEntity.SPEED);

        MainActivity.speedArray = new ArrayList();
        for (int i = 0; i < speedValues.size(); i++) {
            MainActivity.speedArray.add((int)dbValues.get(i).value);
        }

        //Filter only Height values
        List<SensorDataEntity> accValues = new ArrayList<SensorDataEntity>();
        accValues.addAll(dbValues);
        accValues.removeIf(p -> p.type != SensorDataEntity.ACC);

        MainActivity.accArray = new ArrayList();
        for (int i = 0; i < accValues.size(); i++) {
            MainActivity.accArray.add((int) dbValues.get(i).value);
        }

        //Filter only location values
        List<SensorDataEntity> longitudeValues = new ArrayList<SensorDataEntity>();
        longitudeValues.addAll(dbValues);
        longitudeValues.removeIf(p -> p.type != SensorDataEntity.LONGITUDE);
        List<SensorDataEntity> latitudeValues = new ArrayList<SensorDataEntity>();
        latitudeValues.addAll(dbValues);
        latitudeValues.removeIf(p -> p.type != SensorDataEntity.LATITUDE);

        MainActivity.latitudeArray = new float[latitudeValues.size()];
        MainActivity.longitudeArray = new float[latitudeValues.size()];

        for (int i = 0; i < latitudeValues.size(); i++) {
            MainActivity.latitudeArray[i] = (float) latitudeValues.get(i).value;
        }
        for (int i = 0; i < longitudeValues.size(); i++) {
            MainActivity.longitudeArray[i] = (float) longitudeValues.get(i).value;
        }

        onTaskCompletedListener.onTaskCompleted();
    }
}
