/*
 *  This file is part of GlideSafe, an android app aimed to make free flight safer.
 *  Copyright (C) 2020, Clyde Laforge, Kay Lächler, Jessy Ançay
 *
 *     GlideSafe is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     GlideSafe is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GlideSafe.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.example.glidesafe;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {SensorDataEntity.class}, version = 1)
public abstract class GlideSafeRoomDatabase extends RoomDatabase {
    //Abstract class for inheritance: you don't implement the methods but you
    // can extend this class and implement them and add other features

    //Dao to associate to the database and use the queries implemented
    public abstract SensorDataDao sensorDataDao();

    //Instance of the database that will be used later
    private static GlideSafeRoomDatabase INSTANCE;

    //Constructor of the class. It's "synchronized" to avoid that concurrent
    // threads corrupts the instance.
    public static synchronized GlideSafeRoomDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, GlideSafeRoomDatabase.class, "GlideSafeDB").build();
        }
        return INSTANCE;
    }

    //Method to destroy the instance of the database
    public static void destroyInstance() {
        INSTANCE = null;
    }
}

